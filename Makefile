all:
	${CC} momen.c -o momen -lXm -lXt -lcurl ${LDFLAGS} -std=c99 ${CFLAGS} -Wall \
		-Wextra -pedantic
	scdoc < momen.1.scd > momen.1

install:
	mkdir -p /usr/local/bin /usr/local/man
	cp momen /usr/local/bin/momen
	cp momen.1 /usr/local/man/man1/momen.1

uninstall:
	rm /usr/local/bin/momen
	rm /usr/local/man/man1/momen.1
