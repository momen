#include <stdlib.h>
#include <stdio.h>
#include <Xm/Text.h>

void
execute(Widget text_w, XtPointer client_data, XtPointer call_data)
{
	// Discard unused parameters
	(void)client_data;
	(void)call_data;

	char *value = XmTextGetString(text_w);
	char *valuep = (char *) calloc(strlen(value) + 2, sizeof(char));
	sprintf(valuep, "%s &", value);
	printf("Launching %s\n", valuep);
	system(valuep);
	XtFree(value);
	free(valuep);
	exit(0);
}

int
main(int argc, char *argv[])
{
	Widget        toplevel, text_w;
	XtAppContext  app;

	XtSetLanguageProc(NULL, NULL, NULL);
	toplevel = XtVaOpenApplication(&app, "momen", NULL, 0, &argc, argv, NULL,
			sessionShellWidgetClass, NULL);
	text_w = XmCreateText(toplevel, "text", NULL, 0);
	XtManageChild(text_w);
	XtAddCallback(text_w, XmNactivateCallback, execute, NULL);
	XtRealizeWidget(toplevel);
	XtAppMainLoop(app);
}
